import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BitacoraDesaplicacionPagoComponent } from './bitacora-desaplicacion-pago.component';

describe('BitacoraDesaplicacionPagoComponent', () => {
  let component: BitacoraDesaplicacionPagoComponent;
  let fixture: ComponentFixture<BitacoraDesaplicacionPagoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BitacoraDesaplicacionPagoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BitacoraDesaplicacionPagoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
