import { Component, OnInit, ViewChild, Inject, ChangeDetectorRef} from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { AlertService } from '@next/nx-controls-common';
import { MatTableDataSource, MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatCheckboxChange } from '@angular/material';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { ListadoMotivoDesaplicacion } from 'src/app/models/listado-motivo-desaplicacion';
import { DesaplicacionPagoService } from 'src/app/services/desaplicacionPago-service';
import {
  ModalAltaMotivoDesaplicacionComponent
} from 'src/app/components/modal-alta-motivo-desaplicacion/modal-alta-motivo-desaplicacion.component';
import { ListadoAmortizacionesCandidatas } from 'src/app/models/listado-amortizaciones-candidatas';
import { SelectionModel } from '@angular/cdk/collections';
import { ListadoTareas } from 'src/app/models/listado-tareas';
import {
  ModalComentarioAutorizarRechazarComponent
} from '../modal-comentario-autorizar-rechazar/modal-comentario-autorizar-rechazar.component';
import {
  Directive,
  ElementRef,
  AfterViewInit
} from '@angular/core';
/* componentes para agrupar informacion */
export class Group {
  level = 0;
  parent: Group;
  expanded = false;
  check = false;
  fecha = '';
  totalCounts = 0;
  get visible(): boolean {
    return !this.parent || (this.parent.visible && this.parent.expanded);
  }
}

@Component({
  selector: 'app-desaplicacion-pago',
  templateUrl: './desaplicacion-pago.component.html',
  providers: [ModalAltaMotivoDesaplicacionComponent, DesaplicacionPagoService]
})
export class DesaplicacionPagoComponent implements OnInit, AfterViewInit {
  title: string;
  activo: string;
  isDisabled = true;
  selectedRowIndex = '';
  proceso = [];
  /* variables para los botones a utilizar */
  isDisabledSolicitar = true;
  isHiddenSolicitar = true;
  isHiddenAutorizar = true;
  isHiddenApplicar = true;
  isHiddenRechazar = true;
  isHiddenFolioGen = true;
  isHiddenTabSolicitar = true;
  isHiddenResultRever = true;
   /* Fin variables para los botones a utilizar */
  spinner: boolean;
  formulario: FormGroup;
  comboEstatus: string;
  ResulReversa: string;
  capacidad: string;
  folioGen: string;
  codigoCasetera: string;
  resultsLength = 0;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  displayedColumnsFolios: string[] = ['folio', 'tarea', 'atiende', 'fecha',
                                       'asignar tarea'];
  displayedColumnsFoliosAmortizacion: string[] = ['credito' , 'transaccion', 'amortizacion', 'fecha', 'tipo',
                                      'referencia', 'movimiento', 'monto'];
  public dataSourceDesaplicacionPago: MatTableDataSource<ListadoAmortizacionesCandidatas>;
  public dataSourceFolios: MatTableDataSource<ListadoTareas>;
  public dataSourceFolioAmortizacion: MatTableDataSource<ListadoAmortizacionesCandidatas>;
  pageSize: number;
  transaccion: string;
  idReversa: number;
  producto: string;
  cliente: string;
  descripcion: string;
  credito: string;
  fechaInicio: string;
  fechaFin: string;
  ejecucion = false;
  /* componentes para agrupar informacion */
  public dataSource = new MatTableDataSource<any | Group>([]);
  alldata: any[];
  columns: any[];
  displayedColumns: string[] ;
  groupByColumns: string[] = [ ];
  public grupo: Group;
  /* Fin componentes para agrupar Informacion */

  elementoFiltro  = {
    credito: '',
    fechaInicio: '',
    fechaFin: '',
    transaccion: '',
    estatus: null,
    tipoConsulta: '',
    size: 0,
    page: 500,
  };

  elementoBitacora = {
    fecha: '',
    id: 0,
    mensaje: '',
    reversa: {id: 0}
  };
  elementoFiltroSolicitudAmorti  = {
    estatus: 1,
    fecha_pago: '',
    folio: '1',
    id: 0,
    id_credito: '',
    motivo: {id : 0},
    numero_transaccion: '',
    size: 500,
    page: 0,
  };

  elementoAplicaReversa = {
    id: 0,
    id_credito: '',
    numero_transaccion: ''
  };

  elementoFolio = {
    folio: 0
  };

  elementoAsignacionTarea = {
    idNodo: '',
    idProceso: '',
    idTarea: '',
    perfil: 'DESAPLICA_REVIS',
    transicion: 'CONTINUAR'
  };
  elementoNodosTareas = {
    camino: 'CONTINUAR',
    nodoId: '',
    procesoId: '',
    tareaId: ''
  };

  elementoFiltroReversasAmorti  = {
    activo: 0,
    id: 0,
    id_amortizacion: '',
    monto: 0,
    reversa: {id : 0}
};

elementoDesactivaTarea = {
  estatus: 0,
  folio: '0',
  id: 0,
  id_credito: '000000000000000',
  numero_transaccion: '0000000000'
};

elementoAltaEdicion = {
  abreviatura: '',
  activo: false,
  nombre: '',
  id: 0
  };
  public folioSeleccionado: ListadoTareas;
  public listadoFolioAmortizaciones: ListadoAmortizacionesCandidatas;
  public listadolistadoFolioAmortizacion: ListadoAmortizacionesCandidatas[];
  public listadoSolicitudAmorti: ListadoAmortizacionesCandidatas[];
  public listadoFinalSolicitudAmorti: ListadoAmortizacionesCandidatas[];
  public listadoAmortizaciones: ListadoAmortizacionesCandidatas;
  public listadolistadoFinalAmortizaciones: ListadoAmortizacionesCandidatas[];
  public listadolistadoAmortizaciones: ListadoAmortizacionesCandidatas[];
  public listadoMotivosDesaplicacion: ListadoMotivoDesaplicacion;
  public listadolistadoMotivoDesaplicacion: ListadoMotivoDesaplicacion[];
  public listadoTareas: ListadoTareas;
  public listadolistadotareas: ListadoTareas[];

  constructor(private fb: FormBuilder, private alertServices: AlertService,
              private desaplicacionPagoService: DesaplicacionPagoService,
              private cdr: ChangeDetectorRef,
              public dialog: MatDialog) {
              this.formulario = this.fb.group({
                                                motivo: [''],
                                                estatus: [''],
                                                credito: ['', [Validators.required]],
                                                transaccion: [''],
                                                producto: [''],
                                                cliente: [''],
                                                descripcion: [''],
                                                fechaInicio: ['', [Validators.required]],
                                                fechaFin: ['', [Validators.required]]
              });
              this.columns = [
                {
                field: 'transaccion',
                title: 'TRANSACCION'
              }, {
                field: 'amortizacion',
                title: 'AMORTIZACION'
              }, {
                field: 'fecha',
                title: 'FECHA'
              }, {
                field: 'tipo',
                title: 'TIPOS'
              }, {
                field: 'referencia',
                title: 'REFERENCIA'
              }, {
                field: 'descripcion',
                title: 'MOVIMIENTO'
              }, {
                field: 'cantidad',
                title: 'MONTO'
              }];
              this.displayedColumns = this.columns.map(column => column.field);
              this.groupByColumns = ['transaccion'];
            }
selection = new SelectionModel<ListadoTareas>(false, []);
  ngOnInit() {
    this.title = 'Desaplicacion Pago';
    this.comboEstatus = 'true';
    this.listadoFolioAmortizaciones = new ListadoAmortizacionesCandidatas();
    this.listadoMotivosDesaplicacion = new ListadoMotivoDesaplicacion();
    this.listadoAmortizaciones = new ListadoAmortizacionesCandidatas();
    this.listadolistadoAmortizaciones = new Array();
    this.listadolistadoFinalAmortizaciones = new Array();
    this.listadoFinalSolicitudAmorti = new Array();
    this.pageSize = 5;
    this.buscarMotivosActivos();
    this.ConsultaTareas(0, 5);
    this.habilitarBtnsPerfil();
  }
  ngAfterViewInit() {
    if (this.isHiddenSolicitar === true) {
      this.isHiddenTabSolicitar = false;
    }
    this.cdr.detectChanges();
  }
  habilitarBtnsPerfil() {
    /* const clave = [{clave : 'DESAPLICA_SOLIC'},
                  {clave : 'DESAPLICA_REVIS'},
                  {clave : 'DESAPLICA_APROB'},
                  {clave : 'DESAPLICA_RECHA'}  ]; */
    let clave;
    const user = JSON.parse(localStorage.getItem('currentUser'));
    clave = user.perfiles;
    // console.log(clave);
    clave.forEach(element => {

          if (element.clave === 'DESAPLICA_SOLIC') {
            this.isHiddenSolicitar = false;
          }
          if (element.clave === 'DESAPLICA_REVIS') {
            this.isHiddenAutorizar = false;
            this.isHiddenRechazar = false;
          }
          if (element.clave === 'DESAPLICA_APROB') {
            this.isHiddenApplicar = false;
            this.isHiddenRechazar = false;
          }
          if (element.clave === 'DESAPLICA_RECHA') {
            this.isHiddenRechazar = false;
          }
        });
 }

  limpiarInfo() {
    this.dataSourceDesaplicacionPago = new MatTableDataSource<ListadoAmortizacionesCandidatas>();
    this.comboEstatus = 'true';
    this.listadoMotivosDesaplicacion.id = null;
    this.resultsLength = 0;
    this.credito = '';
    this.fechaInicio = '';
    this.fechaFin = '';
    this.transaccion = '';
    this.cliente = '';
    this.descripcion = '';
    this.isDisabled = true;
    this.dataSource = new MatTableDataSource<any | Group>([]);
    this.buscarMotivosActivos();
  }

  buscarMotivosActivos() {
    this.elementoFiltro.estatus = true;
    this.desaplicacionPagoService.consultaMotivosActivos(this.elementoFiltro).subscribe(
      response => {
        console.log(response);
        if (response.returns.data.content.length > 0) {
            this.listadolistadoMotivoDesaplicacion = response.returns.data.content;
            // this.cdr.detectChanges();
        } else {
          this.alertServices.success('No se encontraron motivos para desaplicacion');
        }
      }, error => {
        if (error.error.status === 400) {
          this.alertServices.error(error.errors[0]);
        } else {
          this.alertServices.error(error.error.message);
        }
      }, () => {
      }
    );
  }
buscarAmortizaciones(page: any, size: any) {
    if (this.credito === undefined || this.credito === '') {
      this.elementoFiltro.credito = null;
    } else {
      this.elementoFiltro.credito = this.credito;
    }
    this.elementoFiltro.tipoConsulta = 'S';
    this.elementoFiltro.page = page;
    this.elementoFiltro.size = size;
    this.elementoFiltro.transaccion = this.transaccion;
    this.elementoFiltro.credito = this.credito;
    this.elementoFiltro.fechaInicio = this.convertirFechas(this.fechaInicio);
    this.elementoFiltro.fechaFin = this.convertirFechas(this.fechaFin);
    this.grupo = new Group();
    this.isDisabledSolicitar = true;
    this.isHiddenFolioGen = true;
    this.listadoSolicitudAmorti = new Array();

    this.consultarAmortizacionesReversas(this.elementoFiltro);
}

  consultarAmortizacionesReversas(filtroBusqueda: any) {
    this.desaplicacionPagoService.ConsultaAmortizaciones(filtroBusqueda).subscribe(
      response => {
        console.log(response);
        if (response.length > 0) {
          this.cliente = response[0][0].clieNombre + ' ' + response[0][0].cliApePaterno + ' ' + response[0][0].clieApeMaterno;
          this.descripcion = response[0][0].clieTipDescri;
          this.listadolistadoAmortizaciones = response[1];
          console.log('Amortizaciones a reversas');
          this.listadolistadoAmortizaciones = new Array();
          this.listadolistadoAmortizaciones = response[1];
          console.log(this.listadolistadoAmortizaciones);
          if (response[1].length !== 0) {
            this.dataSource = new MatTableDataSource<any | Group>([]);
            console.log('voy a entrar a Metodo agrupa informacion');
            this.agrupaInformacion(this.listadolistadoAmortizaciones);
            this.isDisabled = false;
         } else {
          console.log('No hay amortizaciones');
          this.alertServices.error('No se encontraron Amortizaciones');
         }
          this.alertServices.success('Consulta exitosa');
          this.cdr.detectChanges();
        } else {
            this.dataSourceDesaplicacionPago = new MatTableDataSource<ListadoAmortizacionesCandidatas>();
            this.alertServices.error(response.message);
        }
      }, error => {
        if (error.error.status === 400) {
          this.spinner = false;
          this.resultsLength = 0;
          this.dataSourceDesaplicacionPago = new MatTableDataSource<ListadoAmortizacionesCandidatas>();
          this.alertServices.error(error.errors[0]);
        } else {
          this.spinner = false;
          this.dataSourceDesaplicacionPago = new MatTableDataSource<ListadoAmortizacionesCandidatas>();
          this.resultsLength = 0;
          this.alertServices.error(error.error.message);
        }
      }, () => {
        this.spinner = false;
      }

    );
  }

  consultaFolioAmortizacionesReversa(folioAmortizacion) {
    this.dataSourceFolioAmortizacion =
            new MatTableDataSource<ListadoAmortizacionesCandidatas>();
    this.desaplicacionPagoService.ConsultaAmortizaciones(folioAmortizacion).subscribe(
      response => {
        // debugger;
        console.log(response);
        if (response.length > 0) {
          this.listadolistadoFolioAmortizacion = response[1];
          if (this.listadolistadoFolioAmortizacion.length !== 0) {
            this.listadolistadoFolioAmortizacion.forEach (amortiFolio => {
              amortiFolio.fecha = amortiFolio.fecha.substring(0, 10);
              amortiFolio.cantidadStr = new Intl.NumberFormat('en-US',
              { style: 'currency', currency: 'USD' }).format(amortiFolio.cantidad);
            });
            this.dataSourceFolioAmortizacion =
            new MatTableDataSource<ListadoAmortizacionesCandidatas>(this.listadolistadoFolioAmortizacion);
            this.alertServices.success('Consulta exitosa');
         } else {
          this.alertServices.error('No se encontraron Amortizaciones');
          this.dataSourceFolioAmortizacion = new MatTableDataSource<ListadoAmortizacionesCandidatas>();
         }
          this.cdr.detectChanges();
        } else {
            this.dataSourceDesaplicacionPago = new MatTableDataSource<ListadoAmortizacionesCandidatas>();
            this.alertServices.success('No se encontraron datos ');
        }
      }, error => {
        // debugger;
        if (error.error.status === 400) {
          this.spinner = false;
          this.resultsLength = 0;
          this.dataSourceFolioAmortizacion =
          new MatTableDataSource<ListadoAmortizacionesCandidatas>();
          this.alertServices.error(error.errors[0]);
        } else {
          this.spinner = false;
          this.dataSourceFolioAmortizacion =
          new MatTableDataSource<ListadoAmortizacionesCandidatas>();
          this.resultsLength = 0;
          this.alertServices.error(error.message);
        }
      }, () => {
        this.spinner = false;
      }
    );

  }

  agrupaInformacion(data: any[] ) {
    console.log('Entre a Metodo agrupa informacion');
    this.listadolistadoFinalAmortizaciones = new Array();
    this.listadolistadoFinalAmortizaciones = data;
    /* this.elementoFiltroSolicitudAmorti.id_credito = this.credito;
    this.elementoFiltroSolicitudAmorti.estatus = 1;
    this.elementoFiltroSolicitudAmorti.folio = undefined;
    this.desaplicacionPagoService.consultaReversasCredito(this.elementoFiltroSolicitudAmorti).subscribe(
    Response => {
        console.log('Voy a eliminar amortizaciones con solicitud');
        data.forEach(element => {
          const amortiExist = Response.returns.data.content.filter(amortizacion => amortizacion.numero_transaccion === element.transaccion);
          if (amortiExist.length === 0) {
            this.listadolistadoFinalAmortizaciones.push(element);
          }
        });
        console.log(this.listadolistadoFinalAmortizaciones);
        console.log('Listo eliminar amortizaciones'); */
        // debugger;
    if (data.length > 0) {
      this.alldata = this.listadolistadoFinalAmortizaciones;
      this.dataSource.data = this.addGroups(this.alldata, this.groupByColumns);
      this.dataSource.filterPredicate = this.customFilterPredicate.bind(this);
      this.dataSource.filter = performance.now().toString();
    } else {
          this.alertServices.error('No se encontraron Transacciones para reversar');
        }
     /* }, error => {
           // debugger;
        if (error.error.status === 400) {
          this.spinner = false;
          this.resultsLength = 0;
          this.dataSourceDesaplicacionPago = new MatTableDataSource<ListadoAmortizacionesCandidatas>();
          this.alertServices.error(error.errors[0]);
        } else { */
         /* this.spinner = false;
          this.dataSourceDesaplicacionPago = new MatTableDataSource<ListadoAmortizacionesCandidatas>();
          this.resultsLength = 0;
          this.alertServices.error(error.message);*/
          /*this.listadolistadoFinalAmortizaciones = data;
          this.alldata = this.listadolistadoFinalAmortizaciones;
          this.dataSource.data = this.addGroups(this.alldata, this.groupByColumns);
          this.dataSource.filterPredicate = this.customFilterPredicate.bind(this);
          this.dataSource.filter = performance.now().toString();*/

        // }
     // }
   // );
  }
  convertirFechas(parametro) {
    console.log(parametro);
    const date = new Date(parametro);
    const  mnth = ('0' + (date.getMonth() + 1)).slice(-2);
    const  day = ('0' + date.getDate()).slice(-2);
    return [date.getFullYear(), mnth, day].join('-') + 'T00:00:00.000Z';
  }

  verAltaMotivoDesaplicacion(titulo: any[], folioSeleccionado: ListadoTareas, idreversa: number) {
    // debugger
    const dialogRefComentarios = this.dialog.open(ModalComentarioAutorizarRechazarComponent, {
      width: '400px',
      height: '310px',
      data: {titulo, folioSeleccionado, idreversa}
    });

    dialogRefComentarios.afterClosed().subscribe(result => {
      this.ConsultaTareas(0 , 5);
      this.dataSourceFolioAmortizacion =
      new MatTableDataSource<ListadoAmortizacionesCandidatas>();
    });
  }

  public rowData(row) {
    /*this.elementoAltaEdicion.id = row.id;
    this.elementoAltaEdicion.nombre = row.nombre;
    this.elementoAltaEdicion.activo = row.activo;
    this.elementoAltaEdicion.abreviatura = row.abreviatura;

    if (this.elementoAltaEdicion.activo === true) {
      this.elementoAltaEdicion.activo = false;
    } else {
      this.elementoAltaEdicion.activo = true;
    }
    this.desaplicacionPagoService.editarMotivoDesaplicacionEstatus(row.id, this.elementoAltaEdicion).subscribe(
      response => {
        if (response.message) {
          this.alertServices.error(response.message);
        } else {
          this.buscarMotivos(0, 5);
          this.alertServices.success('Registro actualizado');
        }
      }, error => {
        console.log(error);
        if (error.error.status === 400) {
          this.alertServices.error(error.errors[0]);
        } else {
          this.alertServices.error(error.error.message);
        }
      });*/
  }

  zfill(numbero: any, width: any) {
   /* const numberOutput = Math.abs(numbero);
    const length = numbero.toString().length;
    const zero = '0';
    if (width <= length) {
        if (numbero < 0) {
             return ('-' + numberOutput.toString());
        } else {
             return numberOutput.toString();
        }
    } else {
        if (numbero < 0) {
            return ('-' + (zero.repeat(width - length)) + numberOutput.toString());
        } else {
            return ((zero.repeat(width - length)) + numberOutput.toString());
        }
    }*/
  }

  pageChanged(e) {
    this.ConsultaTareas(e.pageIndex, e.pageSize);
  }

  groupBy(event, column) {
    event.stopPropagation();
    this.checkGroupByColumn(column.field, true);
    this.dataSource.data = this.addGroups(this.alldata, this.groupByColumns);
    this.dataSource.filter = performance.now().toString();
  }

  checkGroupByColumn(field, add ) {
    let found = null;
    for (const column of this.groupByColumns) {
      if (column === field) {
        found = this.groupByColumns.indexOf(column, 0);
      }
    }
    if (found != null && found >= 0) {
      if (!add) {
        this.groupByColumns.splice(found, 1);
      }
    } else {
      if ( add ) {
        this.groupByColumns.push(field);
      }
    }
  }

  unGroupBy(event, column) {
    event.stopPropagation();
    this.checkGroupByColumn(column.field, false);
    this.dataSource.data = this.addGroups(this.alldata, this.groupByColumns);
    this.dataSource.filter = performance.now().toString();
  }

  // below is for grid row grouping
  customFilterPredicate(data: any | Group, filter: string): boolean {
    return (data instanceof Group) ? data.visible : this.getDataRowVisible(data);
  }

  getDataRowVisible(data: any): boolean {
    const groupRows = this.dataSource.data.filter(
      row => {
        if (!(row instanceof Group)) {
          return false;
        }
        let match = true;
        this.groupByColumns.forEach(column => {
          if (!row[column] || !data[column] || row[column] !== data[column]) {
            match = false;
          }
        });
        return match;
      }
    );

    if (groupRows.length === 0) {
      return true;
    }
    const parent = groupRows[0] as Group;
    return parent.visible && parent.expanded;
  }

  groupHeaderClick(row) {
    row.expanded = !row.expanded;
    this.dataSource.filter = performance.now().toString();  // bug here need to fix
  }

  addGroups(data: any[], groupByColumns: string[]): any[] {
    const rootGroup = new Group();
    rootGroup.expanded = true;
    return this.getSublevel(data, 0, groupByColumns, rootGroup);
  }

  getSublevel(data: any[], level: number, groupByColumns: string[], parent: Group): any[] {
    if (level >= groupByColumns.length) {
      return data;
    }
    const groups = this.uniqueBy(
      data.map(
        row => {
          const result = new Group();
          result.level = level + 1;
          result.parent = parent;
          for (let i = 0; i <= level; i++) {
            result[groupByColumns[i]] = row[groupByColumns[i]];
            result.fecha = row.fecha;
          }
          return result;
        }
      ),
      JSON.stringify);

    const currentColumn = groupByColumns[level];
    let subGroups = [];
    groups.forEach(group => {
      const rowsInGroup = data.filter(row => group[currentColumn] === row[currentColumn]);
      const litera = 'transaccion';
      // debugger;
      if (this.grupo[litera ] !== undefined ) {
        if (this.grupo[litera] === group[ litera ] ) {
          group.check = this.grupo.check;
        } else {
          group.check = !this.grupo.check;
        }
      }
      // debugger;
      // Damos formato al total del agrupado
      group.totalCounts =  new Intl.NumberFormat('en-US',
      { style: 'currency', currency: 'USD' }).format(rowsInGroup.reduce((sum, value) => (sum + value.cantidad), 0 ));

      // Damos formato a cada monto de cada transaccion
      rowsInGroup.forEach (dato => {
        dato.fecha = dato.fecha.substring(0, 10);
        dato.cantidad = new Intl.NumberFormat('en-US',
        { style: 'currency', currency: 'USD' }).format(dato.cantidad);
      });
            // rowsInGroup.length;
      const subGroup = this.getSublevel(rowsInGroup, level + 1, groupByColumns, group);
      subGroup.unshift(group);
      subGroups = subGroups.concat(subGroup);
    });
    return subGroups;
  }

  uniqueBy(a, key) {
    const seen = {};
    return a.filter((item) => {
      const k = key(item);
      return seen.hasOwnProperty(k) ? false : (seen[k] = true);
    });
  }

  isGroup(index, item): boolean {
    return item.level;
  }

  onChkChange(infoD: any, ob: MatCheckboxChange) {
    if ( ob.checked === true) {
      if (this.listadoSolicitudAmorti.length === 0) {
        const amortizacines = this.listadolistadoFinalAmortizaciones.filter(amortizacion => amortizacion.transaccion === infoD.transaccion);
        for (const amorti of amortizacines) {
          this.listadoSolicitudAmorti.push(amorti);
          }
        console.log(this.listadoSolicitudAmorti);
        this.grupo = infoD;
        this.isDisabledSolicitar = false;
      } else {
        // infoD.check = false;
        // this.grupo = infoD;
        this.agrupaInformacion(this.listadolistadoFinalAmortizaciones);
        this.cdr.detectChanges();
        this.alertServices.warn('Solo puede seleccionar Un folio');
      }
      } else {
      this.isDisabledSolicitar = true;
      this.listadoSolicitudAmorti = new Array();
    }
    console.log('Armotizaciones solicitadas: ' + this.listadoSolicitudAmorti);
 }

 generarSolicitud() {
  if (this.listadoMotivosDesaplicacion.id === undefined) {
    this.alertServices.warn('Favor de seleccionar un motivo');
    return;
  }
  this.elementoFiltroSolicitudAmorti.fecha_pago = this.convertirFechas(this.grupo.fecha);
  this.elementoFiltroSolicitudAmorti.id_credito = this.credito;
  this.elementoFiltroSolicitudAmorti.motivo.id  = this.listadoMotivosDesaplicacion.id;
  const literal = 'transaccion';
  this.elementoFiltroSolicitudAmorti.numero_transaccion = this.grupo[literal];
  this.elementoFiltroSolicitudAmorti.folio = '1';
  this.elementoFiltroSolicitudAmorti.estatus = 1;
        // 1.- se genera el folio de la reversa en reversa-credito (cabecera)
  this.desaplicacionPagoService.GeneraFolioSolicitud(this.elementoFiltroSolicitudAmorti).subscribe(
    response => {
      if ( response.errors !== undefined ) {
        this.alertServices.warn(response.errors[0].description);
        return;
      }
      if (response.success !== undefined) {
        if (response.returns.data.length === 0) {
          this.alertServices.warn('No Existen Datos');
          return;
        } else {
          this.elementoFolio.folio =  response.returns.data.folio;
          this.cabeceraAmortizacionSolicitada(this.elementoFolio, response.returns.data.id);
          }
      }
      }, error => {
          console.log(error);
          if (error.error.status === 400) {
            this.spinner = false;
            this.alertServices.error(error.error.internalmessage[0].description);
            return false;
          } else {
            this.spinner = false;
            this.alertServices.error(error.error.message);
            return false;
          }
      }, () => {
              this.spinner = false;
            }
            );
 }

 cabeceraAmortizacionSolicitada(folio: any, idreversa: number) {
  // 2.- recorremos las amortizacioes para mandarlo en grupo
  this.listadoSolicitudAmorti.forEach( elemento => {
    const amortiExist = this.listadoFinalSolicitudAmorti.filter(amortizacion => amortizacion.amortizacion === elemento.amortizacion);
    if (amortiExist.length === 0) {
      const arrayBySuma = this.listadoSolicitudAmorti.filter(amortizacion => amortizacion.amortizacion === elemento.amortizacion);
      // formateamos las cantidades de las amortizaciones seleccionadas
      arrayBySuma.forEach(amorti => {
        // se regresa el valor de la cantidad a numerico, ya que por el format queda como cadena
        const montoParse  = parseFloat((amorti.cantidad + '').replace('$', '').replace(',' , ''));
        console.log(montoParse);
        amorti.cantidad = montoParse;
      });
      const montoAmorti = arrayBySuma.reduce((sum, value) => (sum + value.cantidad), 0 );
      elemento.cantidad = montoAmorti;
      this.listadoFinalSolicitudAmorti.push(elemento);
     }
  });

  // Vamos a recorrer uno a uno cada amortizacion para mandarlo a guardar
  this.listadoFinalSolicitudAmorti.forEach(amortizacion => {
    this.elementoFiltroReversasAmorti.activo = 1;
    this.elementoFiltroReversasAmorti.id_amortizacion = amortizacion.amortizacion;
    this.elementoFiltroReversasAmorti.monto = amortizacion.cantidad;
    this.elementoFiltroReversasAmorti.reversa.id = idreversa;

    this.envioUnoAUnoAmortizaciones(this.elementoFiltroReversasAmorti);
  });
  // debugger;
  this.envioFolioSolicitudBpm(folio);
}

envioUnoAUnoAmortizaciones(elementofiltro: any) {
  this.desaplicacionPagoService.generaReversaAmortizacion(elementofiltro).subscribe(
    response => {
      console.log(response);
    }, error => {
      console.log(error);
      if (error.error.status === 400) {
        this.spinner = false;
        this.alertServices.error(error.error.internalmessage[0].description);
        return false;
      } else {
        this.spinner = false;
        this.alertServices.error(error.error.message);
        return false;
      }
  }, () => {
          this.spinner = false;
        }

   );
}

envioFolioSolicitudBpm(folio) {
 // 3.- se manda el folio de la solicitud para el proceso de BMP
 this.desaplicacionPagoService.altaproceso(folio).subscribe(
  response => {
    console.log(response);
    this.traerProcesosTaras(response.tareaId);
    this.folioGen = folio.folio;
    this.isHiddenFolioGen = false;
  }, error => {
    console.log(error);
    if (error.error.status === 400) {
      this.spinner = false;
      this.alertServices.error(error.error.internalmessage[0].description);
      return false;
    } else {
      this.spinner = false;
      this.alertServices.error(error.error.message);
      return false;
    }
}, () => {
        this.spinner = false;
      }
);
}

traerProcesosTaras(tareaId) {
   // 4.- se trae la informacion de las tareas
   this.desaplicacionPagoService.consultarProcesosTareas(tareaId).subscribe(
    response => {
     console.log(response);
     // 5.- se manda el alta para los nodos y las tareas
     this.elementoNodosTareas.nodoId = response.idNodo;
     this.elementoNodosTareas.procesoId = response.idProceso;
     this.elementoNodosTareas.tareaId = response.idTarea;
     this.altaNodosTareas();
   }, error => {
    console.log(error);
    if (error.error.status === 400) {
      this.spinner = false;
      this.alertServices.error(error.error.internalmessage[0].description);
      return false;
    } else {
      this.spinner = false;
      this.alertServices.error(error.error.message);
      return false;
    }
}, () => {
        this.spinner = false;
      }
    );
}

altaNodosTareas() {
this.desaplicacionPagoService.altaNodoProceso(this.elementoNodosTareas).subscribe(
  response => {
    console.log(response);
    this.limpiarInfo();
    this.ConsultaTareas(0, 5);
    this.alertServices.success('Generacion de folio - Exitosa');
    this.elementoFiltroSolicitudAmorti.id_credito = undefined;
    this.elementoFiltroSolicitudAmorti.estatus = undefined;
    this.elementoFiltroSolicitudAmorti.folio = undefined;
    this.elementoFiltroSolicitudAmorti.fecha_pago = undefined;
  }, error => {
    console.log(error);
    if (error.error.status === 400) {
      this.spinner = false;
      this.alertServices.error(error.error.internalmessage[0].description);
      return false;
    } else {
      this.spinner = false;
      this.alertServices.error(error.error.message);
      return false;
    }
}, () => {
        this.spinner = false;
      }
);
}

 ConsultaTareas(page: any, size: any) {
  this.pageSize = size;
  let asignadas = [];
  let pool = [];
  let originados = [];
  this.desaplicacionPagoService.consultarAllProcesosTareas().subscribe(
      Response => {
        console.log(Response);
        this.listadolistadotareas = new Array();
        if (this.isHiddenSolicitar === false) {
           asignadas = Response.asignadas;
           pool = Response.pool;
           originados = Response.originadas;
        } else if (this.isHiddenAutorizar === false) {
          asignadas = Response.asignadas.filter(row => row.perfil === 'DESAPLICA_REVIS');
          pool = Response.pool.filter(row => row.perfil === 'DESAPLICA_REVIS');
          originados = Response.Originados;
        } else if (this.isHiddenApplicar === false ) {
          asignadas = Response.asignadas.filter(row => row.perfil === 'DESAPLICA_APROB');
          pool = Response.pool.filter(row => row.perfil === 'DESAPLICA_APROB');
          originados = Response.Originados;
        } else if (this.isHiddenRechazar === false ) {
          asignadas = Response.asignadas.filter(row => row.perfil === 'DESAPLICA_RECHA');
          pool = Response.pool.filter(row => row.perfil === 'DESAPLICA_RECHA');
          originados = Response.Originados;
        }
        if ( asignadas === undefined && pool === undefined && originados === undefined) {
          this.alertServices.warn('No se detectaron registros para su perfil');
          return;
        }
        if (asignadas !== undefined) {
          asignadas.forEach(element => {
            const folio = JSON.parse(element.variablesRaw);
            if (folio[0].valor.length <= 14 ) {
              this.listadoTareas = new ListadoTareas();
              this.listadoTareas.folio = folio[0].valor;
              this.listadoTareas.tarea = element.idTarea;
              this.listadoTareas.atiende = element.usuarioAtiende;
              this.listadoTareas.fecha = element.fechaAlta.substring(0, 10);
              this.listadoTareas.solicitante = 'no definido';
              this.listadoTareas.perfil = element.perfil;
              this.listadoTareas.nodo = element.idNodo;
              this.listadoTareas.proceso = element.idProceso;
              this.listadolistadotareas.push(this.listadoTareas);
            }
             });
          }
        if (pool !== undefined) {
            pool.forEach(element => {
              const folio = JSON.parse(element.variablesRaw);
              if (folio[0].valor.length <= 14 ) {
                this.listadoTareas = new ListadoTareas();
                this.listadoTareas.folio = folio[0].valor;
                this.listadoTareas.tarea = element.idTarea;
                this.listadoTareas.atiende = element.usuarioAtiende;
                this.listadoTareas.fecha = element.fechaAlta.substring(0, 10);
                this.listadoTareas.solicitante = 'no definido';
                this.listadoTareas.perfil = element.perfil;
                this.listadoTareas.nodo = element.idNodo;
                this.listadoTareas.proceso = element.idProceso;
                this.listadolistadotareas.push(this.listadoTareas);
              }
               });
          }
        if (originados !== undefined) {
            originados.forEach(element => {
              const folio = JSON.parse(element.variablesRaw);
              if (folio[0].valor.length <= 14 ) {
                this.listadoTareas = new ListadoTareas();
                this.listadoTareas.folio = folio[0].valor;
                this.listadoTareas.tarea = element.idTarea;
                this.listadoTareas.atiende = element.usuarioAtiende;
                this.listadoTareas.fecha = element.fechaAlta.substring(0, 10);
                this.listadoTareas.solicitante = 'no definido';
                this.listadoTareas.perfil = element.perfil;
                this.listadoTareas.nodo = element.idNodo;
                this.listadoTareas.proceso = element.idProceso;
                this.listadolistadotareas.push(this.listadoTareas);
              }
               });
          }
        console.log(this.listadolistadotareas);
        this.dataSourceFolios =
        new MatTableDataSource<ListadoTareas>(this.listadolistadotareas);
        this.cdr.detectChanges();
      }, error => {
        // debugger;
        if (error.error.status === 400) {
          this.spinner = false;
          this.resultsLength = 0;
          // this.dataSourceDesaplicacionPago = new MatTableDataSource<ListadoAmortizacionesCandidatas>();
          this.alertServices.error(error.errors[0]);
        } else {
          this.spinner = false;
          // this.dataSourceDesaplicacionPago = new MatTableDataSource<ListadoAmortizacionesCandidatas>();
          // this.resultsLength = 0;
          this.alertServices.error(error.error.message);
        }
      }

    );
 }

 buscaAmortizacionFolio(infoD: any) {
  this.selection.toggle(infoD);
  console.log('evento row.-' + infoD.folio);
   // debugger;
  this.folioSeleccionado = infoD;
  this.elementoFiltroSolicitudAmorti.folio = infoD.folio;
  this.elementoFiltroSolicitudAmorti.id_credito = undefined;
  this.elementoFiltroSolicitudAmorti.estatus = undefined;
  this.desaplicacionPagoService.consultaReversasCredito(this.elementoFiltroSolicitudAmorti).subscribe(
    Response => {
      // console.log(Response);
      this.idReversa = Response.returns.data.content[0].id;
      this.elementoFiltro.tipoConsulta = 'N';
      this.elementoFiltro.credito = Response.returns.data.content[0].id_credito;
      this.elementoFiltro.fechaInicio = Response.returns.data.content[0].fecha_pago;
      this.elementoFiltro.fechaFin = Response.returns.data.content[0].fecha_pago;
      this.elementoFiltro.transaccion = Response.returns.data.content[0].numero_transaccion;
      this.consultaFolioAmortizacionesReversa(this.elementoFiltro);
     }, error => {
      if (error.error.status === 400) {
        this.spinner = false;
        this.resultsLength = 0;
        // this.dataSourceDesaplicacionPago = new MatTableDataSource<ListadoAmortizacionesCandidatas>();
        this.alertServices.error(error.errors[0]);
      } else {
        this.spinner = false;
        // this.dataSourceDesaplicacionPago = new MatTableDataSource<ListadoAmortizacionesCandidatas>();
        // this.resultsLength = 0;
        this.alertServices.error(error.error.message);
      }
     }
   );
 }

 asignarTarea(infoD: any) {
    this.elementoAsignacionTarea.idNodo = infoD.nodo;
    this.elementoAsignacionTarea.idProceso = infoD.proceso;
    this.elementoAsignacionTarea.idTarea = infoD.tarea;
    const atiende = 'atiende';
    if (infoD[atiende] === undefined) {
      this.desaplicacionPagoService.altaAsignacionUsuarioTarea(this.elementoAsignacionTarea).subscribe(
        Response => {
          // debugger;
          console.log(Response);
          this.alertServices.success(Response.errorMensaje);
          this.ConsultaTareas(0 , 5);
          this.dataSourceFolioAmortizacion =
          new MatTableDataSource<ListadoAmortizacionesCandidatas>();
        }, error => {
          if (error.error.status === 400) {
            this.spinner = false;
            this.resultsLength = 0;
            this.dataSourceFolioAmortizacion =
            new MatTableDataSource<ListadoAmortizacionesCandidatas>();
            this.alertServices.error(error.errors[0]);
          } else {
            this.spinner = false;
            this.dataSourceFolioAmortizacion =
            new MatTableDataSource<ListadoAmortizacionesCandidatas>();
            // this.resultsLength = 0;
            this.alertServices.error(error.error.message);
          }
        }
  );
    } else {
      this.alertServices.warn('La tarea ya esta asignada');
    }
}

 procesoAutorizar() {
   if (this.folioSeleccionado === undefined) {
    this.alertServices.warn('Favor de seleccionar algun Folio para autorizar');
   } else {
    this.proceso = [{tipoTitulo : 'Autorizacion' , proceso: 1 }];
    const atiende = 'atiende';
    console.log(this.folioSeleccionado[atiende]);
    if (this.folioSeleccionado[atiende] !== undefined) {
      if (this.folioSeleccionado.perfil === 'DESAPLICA_REVIS') {
        this.isHiddenResultRever = true;
        this.verAltaMotivoDesaplicacion(this.proceso, this.folioSeleccionado, this.idReversa);
        } else {
          this.alertServices.warn('El folio ya fue Autorizado');
        }
    } else {
      this.alertServices.warn('Para poder continuar favor de asignarse la tarea');
    }
  }
 }

 procesoAplicacion() {
   if (this.folioSeleccionado === undefined) {
    this.alertServices.warn('Favor de seleccionar algun Folio para Aplicar la reversa');
   } else {
  const atiende = 'atiende';
  console.log(this.folioSeleccionado[atiende]);
  if (this.folioSeleccionado[atiende] !== undefined) {
    if (this.folioSeleccionado.perfil === 'DESAPLICA_APROB') {
      this.elementoAplicaReversa.id = this.idReversa;
      this.elementoAplicaReversa.id_credito = this.elementoFiltro.credito;
      this.elementoAplicaReversa.numero_transaccion = this.elementoFiltro.transaccion;
      this.desaplicacionPagoService.aplicaReversa(this.elementoAplicaReversa).subscribe(
     Response => {
      if (Response[0].mensajeError.substring(0, 3) === '000') {
        this.alertServices.success(Response[0].mensajeError);
        this.ResulReversa = Response[0].mensajeError;
        this.isHiddenResultRever = false;
        console.log(Response);
        this.ejecucion = true;
        this.dataSourceFolioAmortizacion =
        new MatTableDataSource<ListadoAmortizacionesCandidatas>();
        this.cambioEstatusNodo(Response[0].mensajeError);
        console.log(this.ejecucion);
      } else {
        this.alertServices.error('Ocurrio un Error al aplicarse la reversa');
      }
    }, error => {
        if (error.error.status === 400) {
          this.spinner = false;
          this.resultsLength = 0;
          this.dataSourceFolioAmortizacion =
            new MatTableDataSource<ListadoAmortizacionesCandidatas>();
          this.alertServices.error(error.errors[0]);
        } else {
          this.spinner = false;
          this.dataSourceFolioAmortizacion =
            new MatTableDataSource<ListadoAmortizacionesCandidatas>();
          this.alertServices.error(error.error.message);
        }
      }, () => {
        this.spinner = false;
      }

   );
    } else {
      this.alertServices.warn('Para poder continuar el folio debe estar autorizado');
    }
  } else {
      this.alertServices.warn('Para poder continuar favor de asignarse la tarea');
  }
   }
}

 cambioEstatusNodo(respuesta: string) {
   // debugger;
  // if (this.ejecucion) {
    this.elementoNodosTareas.nodoId = this.folioSeleccionado.nodo;
    this.elementoNodosTareas.procesoId = this.folioSeleccionado.proceso;
    this.elementoNodosTareas.tareaId = this.folioSeleccionado.tarea;
    // cambio de estatus para finalizar nodo tarea
    this.desaplicacionPagoService.altaNodoProceso(this.elementoNodosTareas).subscribe(
        Response => {
              this.cambioEstatusReversa(respuesta);
        }, error => {
          this.spinner = false;
          this.ejecucion = false;
          this.alertServices.error(error.errors[0]);
          this.dataSourceFolioAmortizacion =
            new MatTableDataSource<ListadoAmortizacionesCandidatas>();
        }, () => {
          this.spinner = false;
        }
    );
}
 cambioEstatusReversa(respuesta: string) {
    // if (this.ejecucion) {
      this.elementoDesactivaTarea.estatus = 2;
      // cambio de estatus para postgres
      this.desaplicacionPagoService.cambioStatus(this.idReversa, this.elementoDesactivaTarea).subscribe(
        Response => {
        this.guardarBitacora(respuesta);
        }, error => {
          this.ejecucion = false;
          this.alertServices.error(error.errors[0]);
          this.dataSourceFolioAmortizacion =
            new MatTableDataSource<ListadoAmortizacionesCandidatas>();
        }, () => {
          this.spinner = false;
        }
        );
  //  }
 }
 guardarBitacora(respuesta: string) {
  const dateDay = new Date();
  this.elementoBitacora.fecha = this.convertirFechasBitacora(dateDay);
  this.elementoBitacora.reversa.id = this.idReversa;
  this.elementoBitacora.mensaje = respuesta;

  this.desaplicacionPagoService.altabitacora(this.elementoBitacora).subscribe(
    Response => {
     this.ConsultaTareas(0, 5);
    }, error => {
      }, () => {

      }
    );

 }

 convertirFechasBitacora(parametro) {
  console.log(parametro);
  console.log(parametro.getTime());
  const date = new Date(parametro);
  const  mnth = ('0' + (date.getMonth() + 1)).slice(-2);
  const  day = ('0' + date.getDate()).slice(-2);
  const hora = ('0' + (date.getHours() )).slice(-2);
  const minuto = ('0' + (date.getMinutes() )).slice(-2);
  const segundo = ('0' + (date.getSeconds() )).slice(-2);
  const tiempo = hora + ':' + minuto + ':' + segundo;
  console.log([date.getFullYear(), mnth, day].join('-') + 'T' + tiempo + '.000Z');
  return [date.getFullYear(), mnth, day].join('-') + 'T' + tiempo + '.000Z';
}

procesoRechazo() {
  if (this.folioSeleccionado === undefined) {
    this.alertServices.warn('Favor de seleccionar algun Folio para Rechazar');
   } else {
    this.proceso = [{tipoTitulo: 'Rechazar', proceso: 2}];
    this.isHiddenResultRever = true;
    this.verAltaMotivoDesaplicacion(this.proceso, this.folioSeleccionado, this.idReversa);
    }

}
}

export interface Element {
  checked: boolean;
  name: string;
  position: number;
  weight: number;
  symbol: string;
  highlighted?: boolean;
  hovered?: boolean;
}


