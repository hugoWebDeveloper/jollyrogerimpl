import { Component, OnInit, ViewChild, Inject, ChangeDetectorRef} from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { AlertService } from '@next/nx-controls-common';
import { MatTableDataSource, MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { ListadoMotivoDesaplicacion } from 'src/app/models/listado-motivo-desaplicacion';
import { DesaplicacionPagoService } from 'src/app/services/desaplicacionPago-service';
import {
  ModalAltaMotivoDesaplicacionComponent
} from 'src/app/components/modal-alta-motivo-desaplicacion/modal-alta-motivo-desaplicacion.component';


@Component({
  selector: 'app-motivo-desaplicacion',
  templateUrl: './motivo-desaplicacion.component.html',
  styleUrls: ['./motivo-desaplicacion.component.scss'],
  providers: [ModalAltaMotivoDesaplicacionComponent, DesaplicacionPagoService]
})
export class MotivoDesaplicacionComponent implements OnInit {
  title: string;
  spinner: boolean;
  motivo: string;
  formulario: FormGroup;
  comboEstatus: string;
  capacidad: string;
  codigoCasetera: string;
  resultsLength = 0;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  displayedColumns: string[] = ['motivo', 'usuarioCreacion', 'fechaCreacion', 'activo', 'acciones'];
  public dataSourceMotivoDesaplicacion: MatTableDataSource<ListadoMotivoDesaplicacion>;
  pageSize: number;


  elementoFiltro  = {
    nombre: '',
    estatus: null,
    size: 0,
    page: 500,
  };

  elementoAltaEdicion = {
    abreviatura: '',
    activo: false,
    nombre: '',
    id: 0
  };
  public listadoMotivosDesaplicacion: ListadoMotivoDesaplicacion;
  public listadolistadoMotivoDesaplicacion: ListadoMotivoDesaplicacion[];
  constructor(private fb: FormBuilder, private alertServices: AlertService,
              private desaplicacionPagoService: DesaplicacionPagoService,
              private cdr: ChangeDetectorRef,
              public dialog: MatDialog) {
                this.formulario = this.fb.group({
      motivo: [''],
      estatus: ['']
      });
  }

  ngOnInit() {
    this.title = 'Motivo Desaplicacion';
    this.comboEstatus = 'true';
    this.listadoMotivosDesaplicacion = new ListadoMotivoDesaplicacion();
    this.listadolistadoMotivoDesaplicacion = new Array();
    this.pageSize = 5;
    this.buscarMotivos(0, 5);
  }

  limpiarInfo() {
    this.dataSourceMotivoDesaplicacion = new MatTableDataSource<ListadoMotivoDesaplicacion>();
    this.motivo = '';
    this.comboEstatus = 'true';
    this.listadoMotivosDesaplicacion.id = null;
    this.resultsLength = 0;
    this.buscarMotivos(0, 5);
  }

  buscarMotivos(page: any, size: any) {
    this.elementoFiltro.page = page;
    this.elementoFiltro.size = size;
    this.desaplicacionPagoService.consultaMotivos(this.elementoFiltro).subscribe(
      response => {
        if (response.returns.data.content.length > 0) {
          this.listadolistadoMotivoDesaplicacion = new Array();
          for (const listadoMotivosDesaplicacion of response.returns.data.content) {
            listadoMotivosDesaplicacion.fechaCreacion = listadoMotivosDesaplicacion.fechaCreacion.substring(0, 10);
            this.listadolistadoMotivoDesaplicacion.push(listadoMotivosDesaplicacion);
          }
          this.resultsLength = response.returns.data.totalElements;
          this.dataSourceMotivoDesaplicacion = new MatTableDataSource<ListadoMotivoDesaplicacion>(this.listadolistadoMotivoDesaplicacion);
          console.log(this.listadolistadoMotivoDesaplicacion);
          this.cdr.detectChanges();
        } else {
          this.alertServices.success('No se encontraron Motivos de desaplicacion');
        }
      }, error => {
        if (error.error.status === 400) {
          this.alertServices.error(error.errors[0]);
        } else {
          this.alertServices.error(error.error.message);
        }
      }, () => {
      }
    );
  }

  buscarMotivosDesaplicacion(page: any, size: any) {

    if (this.motivo === undefined || this.motivo === '') {
      this.elementoFiltro.nombre = null;
    } else {
      this.elementoFiltro.nombre = this.motivo;
    }
    this.pageSize = size;
    this.elementoFiltro.page = page;
    this.elementoFiltro.size = size;
    if (this.comboEstatus === 'true') {
      this.elementoFiltro.estatus = true;
    } else {
      this.elementoFiltro.estatus = false;
    }

    this.desaplicacionPagoService.ConsultaMotivosDesaplicacion(this.elementoFiltro).subscribe(
      response => {
        console.log(response);
        if (response.success !== undefined) {
          if (response.returns.data.content.length !== 0) {
            this.listadolistadoMotivoDesaplicacion = new Array();
            for (const listadoMotivosDesaplicacion of response.returns.data.content) {
              listadoMotivosDesaplicacion.fechaCreacion = listadoMotivosDesaplicacion.fechaCreacion.substring(0, 10);
              this.listadolistadoMotivoDesaplicacion.push(listadoMotivosDesaplicacion);
            }
            this.resultsLength = response.returns.data.totalElements;
            this.dataSourceMotivoDesaplicacion =
            new MatTableDataSource<ListadoMotivoDesaplicacion>(this.listadolistadoMotivoDesaplicacion);
            console.log(this.listadolistadoMotivoDesaplicacion);
            this.cdr.detectChanges();
          }
        } else {
            this.dataSourceMotivoDesaplicacion = new MatTableDataSource<ListadoMotivoDesaplicacion>();
            this.alertServices.success('No se encontraron datos ');
        }
      }, error => {
        if (error.error.status === 400) {
          this.spinner = false;
          this.resultsLength = 0;
          this.dataSourceMotivoDesaplicacion = new MatTableDataSource<ListadoMotivoDesaplicacion>();
          this.alertServices.error(error.errors[0]);
        } else {
          this.spinner = false;
          this.dataSourceMotivoDesaplicacion = new MatTableDataSource<ListadoMotivoDesaplicacion>();
          this.resultsLength = 0;
          this.alertServices.error(error.error.message);
        }
      }, () => {
        this.spinner = false;
      }
    );
  }

  verAltaMotivoDesaplicacion(accionD: any, infoD: any) {
    const dialogRefComentarios = this.dialog.open(ModalAltaMotivoDesaplicacionComponent, {
      width: '400px',
      height: '450px',
      data: {info: infoD, accion: accionD}
    });

    dialogRefComentarios.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      this.buscarMotivos(0, 5);
    });
  }

  public rowData(row) {
    this.elementoAltaEdicion.id = row.id;
    this.elementoAltaEdicion.nombre = row.nombre;
    this.elementoAltaEdicion.activo = row.activo;
    this.elementoAltaEdicion.abreviatura = row.abreviatura;

    if (this.elementoAltaEdicion.activo === true) {
      this.elementoAltaEdicion.activo = false;
    } else {
      this.elementoAltaEdicion.activo = true;
    }
    this.desaplicacionPagoService.editarMotivoDesaplicacionEstatus(row.id, this.elementoAltaEdicion).subscribe(
      response => {
        if (response.message) {
          this.alertServices.error(response.message);
        } else {
          this.buscarMotivos(0, 5);
          this.alertServices.success('Registro actualizado');
        }
      }, error => {
        console.log(error);
        if (error.error.status === 400) {
          this.alertServices.error(error.errors[0]);
        } else {
          this.alertServices.error(error.error.message);
        }
      });
  }

  zfill(numbero: any, width: any) {
    const numberOutput = Math.abs(numbero);
    const length = numbero.toString().length;
    const zero = '0';
    if (width <= length) {
        if (numbero < 0) {
             return ('-' + numberOutput.toString());
        } else {
             return numberOutput.toString();
        }
    } else {
        if (numbero < 0) {
            return ('-' + (zero.repeat(width - length)) + numberOutput.toString());
        } else {
            return ((zero.repeat(width - length)) + numberOutput.toString());
        }
    }
  }

  pageChanged(e) {
    this.buscarMotivosDesaplicacion(e.pageIndex, e.pageSize);
  }



}
