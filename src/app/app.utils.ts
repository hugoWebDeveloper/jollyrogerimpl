import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';

declare var Materialize: any;
declare var $: any;

@Injectable()
export class AppUtil {

  constructor(
    private cookieService: CookieService
  ) { }

  /**
   * Funcion estatica para mostrar mensaje de error en pantalla
   * @param mensaje Mensaje que se muestra en el popup de error
   */
  static showErrorMsg(mensaje: string) {
    alert(mensaje);
  }

  /**
   * Funcion estatica para mostrar mensaje de confirmacion en pantalla
   * @param mensaje Mensaje que se muestra en el popup de confirmacion
   */
  static showConfirmationMsg(mensaje: string): any {
    alert(mensaje);
  }

  /**
   * Funcion estatica para mostrar mensaje de accion de exito
   * @param mensaje Mensaje de accion realizada con exito en el popup de exito
   */
  static showSuccessMsg(mensaje: string) {
    alert(mensaje);
  }

  /**
   * Funcion estatica para la notificacion de Usuario de Aplicacion No Valido
   */
  static showUsuarioNoValido() {
    alert('Usuario No Válido!');
  }

  /**
   * Funcion para recuperar informacion del usuario que ha ingresado a la herramienta
   */
  getCurrentUser() {
    const currentUser = localStorage.getItem('currentUser');
    if (currentUser) {
        return JSON.parse(currentUser);
    } else {
        return '';
    }
  }
}
