export class ListadoMotivoDesaplicacion {
    id: number;
    nombre: string;
    abreviatura: string;
    activo: boolean;
    usuarioCreacion: string;
    fechaCreacion: string;
}
