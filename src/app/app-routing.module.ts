import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { AuthGuard, LoginComponent } from '@next/nx-core';
import { MotivoDesaplicacionComponent } from './components/motivo-desaplicacion/motivo-desaplicacion.component';
import { DesaplicacionPagoComponent } from './components/desaplicacion-pago/desaplicacion-pago.component';
import { ReporteDesaplicacionPagoComponent } from './components/reporte-desaplicacion-pago/reporte-desaplicacion-pago.component';
import { BitacoraDesaplicacionPagoComponent } from './components/bitacora-desaplicacion-pago/bitacora-desaplicacion-pago.component';
import { EmpleadosComponent } from './components/empleados/empleados.component';
import { JuegoComponent } from './components/juego/juego.component';
import { MiLoginComponent } from './components/mi-login/mi-login.component';
import { AuthGuardService } from './services/auth-guard.service';
import { NoAutorizadoComponent } from './components/no-autorizado/no-autorizado.component';

/*const routes: Routes = [
{path: '', component: HomeComponent , canActivate: [AuthGuard]},
 {path: 'login', component: LoginComponent },
  {
   path: 'home', component: HomeComponent,
   children: [
     { path: 'motivoDesaplicacion', component: MotivoDesaplicacionComponent },
     { path: 'desaplicacionPago', component: DesaplicacionPagoComponent },
     { path: 'reporteDesaplicacion', component: ReporteDesaplicacionPagoComponent},
     { path: 'bitacorasDesaplicacion', component: BitacoraDesaplicacionPagoComponent},
     { path: '**', redirectTo: '/home', pathMatch: 'full' }
   ], canActivate: [AuthGuard]
  }
];*/
const routes: Routes = [
  {
    path: '', component: MiLoginComponent
  },
  {
    path: 'milogin', component: MiLoginComponent
  },
  {
    path: 'home', component: HomeComponent,
    canActivateChild: [AuthGuardService],
    children: [
      { path: 'empleado', component: EmpleadosComponent, data: { role: 'empleado' }, canActivate: [AuthGuardService] },
      { path: 'juego', component: JuegoComponent, data: { role: 'jugador' }, canActivate: [AuthGuardService] }
    ]
  },
  { path: 'noAutorizado', component: NoAutorizadoComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: false })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
