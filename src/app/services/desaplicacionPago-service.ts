import { Injectable } from '@angular/core';
// import { Observable } from 'rxjs/Rx';
import { Observable } from 'rxjs';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';

@Injectable()
export class DesaplicacionPagoService {
    private url;
    private urlReporte;
    private urlCatalogos;
    private token;

    constructor(private http: HttpClient) {
        this.url = 'desaplicacion-pagos/';
        // this.url = 'http://localhost:8080/';
        // this.urlReporte = 'http://localhost:8084/';
        this.urlReporte = 'reportes/';
    }
    public ConsultaMotivosDesaplicacion(filtroMotivoDesaplicacion: any): Observable<any> {
        let params;
        params = new HttpParams();
        if (filtroMotivoDesaplicacion.nombre !== undefined && filtroMotivoDesaplicacion.nombre !== null) {
                params = params.set('nombre', filtroMotivoDesaplicacion.nombre);
            }
        if (filtroMotivoDesaplicacion.estatus !== undefined) {
                params = params.set('activo', filtroMotivoDesaplicacion.estatus);
            }
        params = params.set('page', filtroMotivoDesaplicacion.page);
        params = params.set('size', filtroMotivoDesaplicacion.size);
        return this.http.get<any>(this.url + 'motivos', {params,
        headers: {'Content-Type': 'application/json'}});
    }
    public ConsultaAmortizaciones(filtroMotivoDesaplicacion: any): Observable<any> {
        let params;
        params = new HttpParams();
        params = params.set('credito', filtroMotivoDesaplicacion.credito);
        params = params.set('fechaIni', filtroMotivoDesaplicacion.fechaInicio);
        params = params.set('fechaFin', filtroMotivoDesaplicacion.fechaFin);
        params = params.set('tipoConsulta', filtroMotivoDesaplicacion.tipoConsulta);
        if (filtroMotivoDesaplicacion.transaccion !== undefined) {
            params = params.set('transaccio', filtroMotivoDesaplicacion.transaccion);
        }
        params = params.set('page', filtroMotivoDesaplicacion.page);
        params = params.set('size', filtroMotivoDesaplicacion.size);
        return this.http.get<any>(this.url + 'amortizaciones', {params,
        headers: {'Content-Type': 'application/json'}});
    }
    public aplicaReversa(aplicaReversa: any): Observable<any> {
        return this.http.post<any>(this.url + 'amortizaciones', aplicaReversa, { headers: {'Content-Type': 'application/json'}});
    }
    public GeneraFolioSolicitud(generafoliosolicitud: any): Observable<any> {
        return this.http.post<any>(this.url + 'reversas-credito', generafoliosolicitud, { headers: {'Content-Type': 'application/json'}});
    }

    public generaReversaAmortizacion(generareversaamortizacion: any): Observable<any> {
         return this.http.post<any>(this.url + 'amortizaciones/reversas-credito',
         generareversaamortizacion, { headers: {'Content-Type': 'application/json'}});
    }
    public consultaReversasCredito(filtroReversasCredito: any): Observable<any> {
        let params;
        params = new HttpParams();
        if (filtroReversasCredito.folio !== undefined) {
            params = params.set('folio', filtroReversasCredito.folio);
        }
        if (filtroReversasCredito.id_credito !== undefined) {
            params = params.set('id_credito', filtroReversasCredito.id_credito);
        }
        if (filtroReversasCredito.estatus !== undefined) {
            params = params.set('estatus', filtroReversasCredito.estatus);
        }
        params = params.set('page', filtroReversasCredito.page);
        params = params.set('size', filtroReversasCredito.size);
        return this.http.get<any>(this.url + 'reversas-credito',
        {params, headers: {'Content-Type': 'application/json'}});

    }

    public consultaMotivos(filtroMotivoDesaplicacion: any): Observable<any> {
        let params;
        params = new HttpParams();
        params = params.set('page', filtroMotivoDesaplicacion.page);
        params = params.set('size', filtroMotivoDesaplicacion.size);
        return this.http.get<any>(this.url + 'motivos',
        {params, headers: {'Content-Type': 'application/json'}});
    }

    public consultaMotivosActivos(filtroMotivoDesaplicacion: any): Observable<any> {
        let params;
        params = new HttpParams();
        params = params.set('activo', filtroMotivoDesaplicacion.estatus);
        return this.http.get<any>(this.url + 'motivos',
        {params, headers: {'Content-Type': 'application/json'}});
    }

    public altaMotivoDesaplicacion(altaPorcentajeAceptable: any): Observable<any> {
        return this.http.post<any>(this.url + 'motivos', altaPorcentajeAceptable,
        {headers: {'Content-Type': 'application/json'}} );
    }

    public altaproceso(altaproceso: any): Observable<any> {
        return this.http.post<any>(this.url + 'procesos', altaproceso,
        {headers: {'Content-Type': 'application/json'}} );
    }

    public altaNodoProceso(altaNodoProceso: any): Observable<any> {
        return this.http.post<any>(this.url + 'procesos/nodos-tareas', altaNodoProceso,
        {headers: {'Content-Type': 'application/json'}} );
    }

    public cambioStatus(id, edicionEstatus: any ): Observable<any> {
        return this.http.put<any>(this.url + 'reversas-credito/' + id, edicionEstatus,
        {headers: {'Content-Type': 'application/json'}} );
    }

    public altabitacora(atabitacora: any): Observable<any> {
        return this.http.post<any>(this.url + 'bitacoras', atabitacora,
        {headers: {'Content-Type': 'application/json'}} );
    }

    public altaAsignacionUsuarioTarea(altaAsignacionUsuarioTarea: any): Observable<any> {
        return this.http.post<any>(this.url + 'procesos/usuarios-tareas', altaAsignacionUsuarioTarea,
        {headers: {'Content-Type': 'application/json'}} );
    }

    public consultarAllProcesosTareas() {
        return this.http.get<any>(this.url + 'procesos/tareas/', {headers: {'Content-Type': 'application/json'}} );
    }

    public consultarProcesosTareas(id: any) {
        return this.http.get<any>(this.url + 'procesos/tareas/' + id, {headers: {'Content-Type': 'application/json'}} );
    }



    public editarMotivoDesaplicacion(id, edicionPocentajeAceptable: any): Observable<any> {
        return this.http.put<any>(this.url + 'motivos/' + id, edicionPocentajeAceptable,
        {headers: {'Content-Type': 'application/json'}} );
    }

    public editarMotivoDesaplicacionEstatus(id, edicionPocentajeAceptable: any): Observable<any> {
        return this.http.put<any>(this.url + 'motivos/' + id, edicionPocentajeAceptable,
        {headers: {'Content-Type': 'application/json'}} );
    }

    public ConsultaReporteReversa(filtroReporteReversa: any): Observable<any> {
        let params;
        params = new HttpParams();
        if (filtroReporteReversa.credito !== undefined  && filtroReporteReversa.credito !== null) {
            params = params.set('credito', filtroReporteReversa.credito);
        }
        if (filtroReporteReversa.fechaInicio !== undefined && filtroReporteReversa.fechaInicio !== null ) {
            params = params.set('fechaIni', filtroReporteReversa.fechaInicio);
        }
        if (filtroReporteReversa.fechaFin !== undefined && filtroReporteReversa.fechaFin !== null) {
            params = params.set('fechaFin', filtroReporteReversa.fechaFin);
        }
        return this.http.get<any>(this.urlReporte + 'reportes/creditos-reversados', {params,
        headers: {'Content-Type': 'application/json'}});
    }
    public ConsultaBitacoraReversa(filtroBitacoraReversa: any): Observable<any> {
        let params;
        params = new HttpParams();
        if (filtroBitacoraReversa.fechaInicio !== undefined && filtroBitacoraReversa.fechaInicio !== null ) {
            params = params.set('fechaInicio', filtroBitacoraReversa.fechaInicio);
        }
        if (filtroBitacoraReversa.fechaFin !== undefined && filtroBitacoraReversa.fechaFin !== null) {
            params = params.set('fechaFin', filtroBitacoraReversa.fechaFin);
        }
        return this.http.get<any>(this.url + 'bitacoras', {params,
        headers: {'Content-Type': 'application/json'}});
    }
}
